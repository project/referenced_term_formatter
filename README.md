About
-----

The "Referenced Term Formatter" module enhances the display of taxonomy
term references in Drupal 9/10, providing powerful customization options
for presenting terms associated with content entities. This module is
designed to offer flexibility in formatting, allowing you to control
the representation of referenced terms with ancestors based on your
specific requirements.

### Key Features

*   ###### Custom Ancestor Parent Levels:
    
    Define the depth of term hierarchy to be displayed, offering
    flexibility in showcasing only the relevant parent ancestors.
    Configure the number of ancestor parent levels to be shown,
    controlling the granularity of the displayed hierarchy for
    optimal content clarity.
*   ###### Link or Text Only:
    
    Choose between displaying referenced terms as hyperlinks or plain
    text, catering to diverse design preferences and user experiences.
*   ###### Custom Separator:
    
    Tailor the separator between parent ancestors, allowing for a
    visually appealing and cohesive presentation of term hierarchies.

### Use Cases

*   ###### Content Pages:
    
    Enhance the readability and aesthetic appeal of content pages by
    presenting taxonomy terms in a structured and visually consistent manner.
*   ###### Custom Views:
    
    Tailor views and displays for specific content types, offering a
    cohesive and polished taxonomy representation.
*   ###### User Profiles:
    
    Improve the user experience by displaying relevant term hierarchies
    in user profiles, aiding in content navigation.

### How to Use

*   ###### Enable the Module:
    
    After installation, enable the "Referenced Term Formatter" module
    in the Drupal administration interface.
*   ###### Configure Display Settings:
    
    Navigate to the "Manage Display" settings for the content type
    containing your taxonomy term reference field.
*   ###### Choose the Formatter:
    
    Select "Separated Terms Formatter" as the formatter for your taxonomy
    term reference field.
*   ###### Customize Options:
    
    Fine-tune the display by configuring options such as hierarchy depth,
    link or text-only representation, displaying referenced terms as
    hyperlinks or plain text and the desired separator.
