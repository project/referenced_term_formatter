<?php

namespace Drupal\referenced_term_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'custom_taxonomy_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_taxonomy_formatter",
 *   label = @Translation("Separated Terms Formatter"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class SeparatedTermsFormatter extends FormatterBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a SeparatedTermsFormatter object.
   *
   * @param string $plugin_id
   *   The plugin ID for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // Check if the field is an entity reference field targeting taxonomy terms.
    return $field_definition->getSetting('target_type') == 'taxonomy_term';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'parent_level' => '3',
      'parent_with_link' => TRUE,
      'separator' => '>',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['parent_level'] = [
      '#title' => $this->t('Parent level'),
      '#type' => 'select',
      '#options' => [
        '1' => $this->t('1'),
        '2' => $this->t('2'),
        '3' => $this->t('3'),
        '4' => $this->t('4'),
        '5' => $this->t('5'),
        '6' => $this->t('6'),
        'unlimited' => $this->t('Unlimited'),

      ],
      '#default_value' => $this->getSetting('parent_level'),
      '#description' => $this->t('Display the level of ancestors for a specific taxonomy term.'),
    ];
    $elements['parent_with_link'] = [
      '#title' => $this->t('Parent with link'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('parent_with_link'),
      '#description' => $this->t('Display the ancestor taxonomy terms with link.'),
    ];
    $elements['separator'] = [
      '#title' => $this->t('Separator'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('separator'),
      '#description' => $this->t('Display ancestor taxonomy terms separated by a specific separator.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary['dimensions'] = $this->t('<strong>Parent level:</strong> @parent_level
			<br><strong>Display terms with link:</strong> @parent_with_link
			<br><strong>Separator:</strong> @separator', [
     '@parent_level' => $this->getSetting('parent_level'),
     '@parent_with_link' => $this->getSetting('parent_with_link') ? 'True' : 'False',
     '@separator' => $this->getSetting('separator'),
   ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $parent_with_link = $this->getSetting('parent_with_link');
    $parent_level = $this->getSetting('parent_level');
    $separator = $this->getSetting('separator');

    if ($items) {
      foreach ($items as $delta => $item) {
        if (isset($item->target_id)) {
          $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($item->target_id);
          if ($term) {

            $term_id = $term->id();

            // Get the TermStorage service.
            $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

            $parent_terms = [];

            $count = 0;
            foreach ($term_storage->loadAllParents($term_id) as $parent_term) {
              if ($parent_term) {
                if ($count <= $parent_level) {
                  if ($parent_with_link) {

                    // Get the URL for the taxonomy term.
                    $term_url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $parent_term->id()]);

                    // Create a link to the taxonomy term.
                    $term_link = Link::fromTextAndUrl($parent_term->label(), $term_url);

                    // Add the term link to the array.
                    $parent_terms[] = $term_link->toString();
                  }
                  else {
                    // Add the term name to the array.
                    $parent_terms[] = $parent_term->getName();
                  }
                }
                else {
                  break;
                }
                $count++;
              }
            }

            // Reverse the array to display the hierarchy from top to bottom.
            $parent_terms = array_reverse($parent_terms);

            // Concatenate the parent names with a separator
            // (you can customize this).
            $hierarchy = implode(" {$separator} ", $parent_terms);

            $elements[$delta] = [
              '#markup' => $hierarchy,
            ];
          }
        }
      }
    }

    return $elements;
  }

}
